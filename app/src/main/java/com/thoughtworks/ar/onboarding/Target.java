/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.thoughtworks.ar.onboarding;

import android.graphics.Bitmap;


// A support class encapsulating the info for one book
public class Target {
    private String title;
    private Bitmap thumbnail;
    private String bookUrl;


    public Target() {
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public Bitmap getThumbnail() {
        return thumbnail;
    }


    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }


    public String getBookUrl() {
        return bookUrl;
    }


    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }
}
